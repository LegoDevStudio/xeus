package com.avairebot.commands.pinewood;

import com.avairebot.AvaIre;
import com.avairebot.commands.CommandMessage;
import com.avairebot.contracts.commands.Command;
import com.avairebot.contracts.commands.CommandGroup;
import com.avairebot.contracts.commands.CommandGroups;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;

import javax.annotation.Nonnull;
import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HandbookCommand extends Command {
    public HandbookCommand(AvaIre avaire) {
        super(avaire);
    }


    @Override
    public String getName() {
        return "Handbook Command";
    }

    @Override
    public String getDescription() {
        return "Show's a menu with all the handbooks.";
    }

    @Override
    public List <String> getUsageInstructions() {
        return Arrays.asList(
            "`:command` - Show's the handbooks.",
            "`:command <pia/tms/pbst/pet>` - Show's the defined handbook"
        );
    }

    @Override
    public List <String> getExampleUsage() {
        return Arrays.asList(
            "`:command` - Show's the handbooks.",
            "`:command <pia/tms/pbst/pet>` - Show's the defined handbook"
        );
    }


    @Override
    public List <String> getTriggers() {
        return Collections.singletonList("handbook");
    }

    @Nonnull
    @Override
    public List <CommandGroup> getGroups() {
        return Collections.singletonList(
            CommandGroups.MISCELLANEOUS
        );
    }

    @Override
    public List <String> getMiddleware() {
        return Arrays.asList(
            "throttle:user,1,30",
            "throttle:guild,1,15"
        );
    }

    @Override
    public boolean onCommand(CommandMessage context, String[] args) {
        context.makeEmbeddedMessage(Color.GREEN).setDescription("**Welcome to the handbook selection menu**!\n\n" +
            " Please select one of these handbooks:\n" +
            " - <:PBSTHandbook:690133745805819917> (**[PBST](https://pbst.pinewood-builders.com/)**)\n" +
            " - <:TMSHandbook:690134424041422859> (**[TMS](https://tms.pinewood-builders.com/)**)\n" +
            " - <:PETHandbook:690134297465585704> (**[PET](https://pet.pinewood-builders.com/)**)").setFooter("This embed is owned by: " + context.getMember().getNickname()).queue(message -> {
            addReactions(message);
            startWaiter(message, context);
        });

        // system.out.println("This is the end!");
        return true;
    }

    private void startWaiter(Message message, CommandMessage context) {
        avaire.getWaiter().waitForEvent(GuildMessageReactionAddEvent.class, u -> context.getMember().equals(u.getMember()) && isMessage(message.getId(), u.getMessageId()), reactionMessage -> {
            sendFinalMessage(reactionMessage, message, context);
        }, 20, TimeUnit.SECONDS, () -> {
            message.editMessage(context.makeWarning("<a:alerta:729735220319748117> You've taken to long to react to the message <a:alerta:729735220319748117>").setFooter("This embed is owned by: " + context.getMember().getNickname() + " | Remove in 10s").buildEmbed()).queue();
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            message.delete().queue();
        });
    }


    private void startPETWaiter(Message message, CommandMessage context) {
        avaire.getWaiter().waitForEvent(GuildMessageReactionAddEvent.class, u -> context.getMember().equals(u.getMember()) && isMessage(message.getId(), u.getMessageId()), reactionMessage -> {
            sendFinalPETMessage(reactionMessage, message, context);
        }, 30, TimeUnit.SECONDS, () -> {
            message.editMessage(context.makeWarning("<a:alerta:729735220319748117> You've taken to long to react to the message <a:alerta:729735220319748117>").setFooter("This embed is owned by: " + context.getMember().getNickname() + " | Remove in 10s").buildEmbed()).queue();
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            message.delete().queue();
        });
    }

    private void sendFinalPETMessage(GuildMessageReactionAddEvent rM, Message m, CommandMessage c) {
        m.clearReactions().queue();
        // system.out.println(rM.getReactionEmote().getName());
        switch (rM.getReactionEmote().getName()) {
            case "PETMedic":
                m.editMessage(c.makeEmbeddedMessage().setColor(new Color(0, 71, 105)).setDescription("**[Medic Handbook:](https://pet.pinewood-builders.com/pet/medic/)**\n" +
                    "[Medical Team Rules](https://pet.pinewood-builders.com/pet/medic/#medical-team-rules)\n" +
                    "    - [(MT. 1) Always have a med kit](https://pet.pinewood-builders.com/pet/medic/#mt-1-always-have-a-med-kit)\n" +
                    "    - [(MT. 2) Heal everyone (Except enemies)](https://pet.pinewood-builders.com/pet/medic/#mt-1-always-have-a-med-kit)\n" +
                    "    - [(MT. 3) No combat healing](https://pet.pinewood-builders.com/pet/medic/#mt-3-no-combat-healing)\n" +
                    "[Medical Team Combat Medic Sub-Division](https://pet.pinewood-builders.com/pet/medic/#medical-team-combat-medic-sub-division)\n" +
                    "    - [(MT.4) PVP Heal](https://pet.pinewood-builders.com/pet/medic/#mt-4-pvp-heal)\n" +
                    "    - [(MT.4A) BE CAREFULL](https://pet.pinewood-builders.com/pet/medic/#mt-4a-be-carefull)\n").setFooter("This embed is owned by: " + c.getMember().getNickname()).buildEmbed()).queue();
                break;
            case "PETFire":
                m.editMessage(c.makeEmbeddedMessage(new Color(172, 41, 0)).setDescription("**[Fire Handbook:](https://pet.pinewood-builders.com/pet/fire/)**\n" +
                    "[Fire Team Rules](https://pet.pinewood-builders.com/pet/fire/#fire-team-rules)\n" +
                    "[Points](https://pet.pinewood-builders.com/pet/fire/#points)\n" +
                    "[Rank system](https://pet.pinewood-builders.com/pet/fire/#rank-system)\n" +
                    " - [Firefighter](https://pet.pinewood-builders.com/pet/fire/#firefighter)\n" +
                    " - [Trained Firefighter](https://pet.pinewood-builders.com/pet/fire/#trained-firefighter)\n" +
                    " - [Experienced Firefighter](https://pet.pinewood-builders.com/pet/fire/#experienced-firefighter)\n" +
                    " - [Elite Firefighter](https://pet.pinewood-builders.com/pet/fire/#elite-firefighter)\n" +
                    " - [Fire Marshall (Handpicked)](https://pet.pinewood-builders.com/pet/fire/#fire-marshall-handpicked)\n" +
                    " - [Deputy Chief (Handpicked by Fire Chief)](https://pet.pinewood-builders.com/pet/fire/#deputy-chief-handpicked-by-fire-chief)\n" +
                    "[Self Patrol](https://pet.pinewood-builders.com/pet/fire/#self-patrol)\n" +
                    " - [What is it?](https://pet.pinewood-builders.com/pet/fire/#what-is-it)\n" +
                    " - [How will it work?](https://pet.pinewood-builders.com/pet/fire/#how-will-it-work)\n" +
                    " - [Earn points without a training](https://pet.pinewood-builders.com/pet/fire/#earn-points-without-a-training)\n" +
                    "[PET Fire History](https://pet.pinewood-builders.com/pet/fire/#pet-fire-history)\n" +
                    " - [Creation and chiefs](https://pet.pinewood-builders.com/pet/fire/#creation-and-chiefs)\n" +
                    " - [Fire HR's](https://pet.pinewood-builders.com/pet/fire/#fire-hr-s)\n" +
                    "[Feedback by our (former) members](https://pet.pinewood-builders.com/pet/fire/#feedback-by-our-former-members)").setFooter("This embed is owned by: " + c.getMember().getNickname()).buildEmbed()).queue();
                break;
            case "PETHazmat":
                m.editMessage(c.makeEmbeddedMessage(new Color(209, 227, 0)).setDescription("**[Hazmat Handbook:](https://pet.pinewood-builders.com/pet/hazmat/)**\n" +
                    "[Hazmat Team Rules](https://pet.pinewood-builders.com/pet/hazmat/#hazmat-team-rules)\n" +
                    " - [(HT. 1) Wear suit at all times](https://pet.pinewood-builders.com/pet/hazmat/#ht-1-wear-suit-at-all-times)\n" +
                    " - [(HT. 2) Be there for everyone](https://pet.pinewood-builders.com/pet/hazmat/#ht-2-be-there-for-everyone)\n" +
                    " - [(HT. 3) Access to fans in disaster](https://pet.pinewood-builders.com/pet/hazmat/#ht-3-access-to-fans-in-disaster)\n" +
                    " - [(HT. 4) Only heat/cool core for save](https://pet.pinewood-builders.com/pet/hazmat/#ht-4-only-heat-cool-core-for-save)\n")
                    .setFooter("This embed is owned by: " + c.getMember().getNickname()).buildEmbed()).queue();
                break;
            default:
                m.editMessage(c.makeError(":no_entry_sign: This emoji is invalid. Please execute the command again and select a correct emoji.").buildEmbed()).queue();
                break;

        }
    }


    private void sendFinalMessage(GuildMessageReactionAddEvent rM, Message m, CommandMessage c) {
        m.clearReactions().queue();
        // system.out.println(rM.getReactionEmote().getName());
        switch (rM.getReactionEmote().getName()) {
            case "PBSTHandbook":
                m.editMessage(c.makeEmbeddedMessage(Color.BLUE).setDescription("[PBST Handbook](https://pbst.pinewood-builders.com/handbook)\n" +
                    "[Going on duty to patrol](https://pbst.pinewood-builders.com/handbook/#going-on-duty-to-patrol)\n" +
                    "- [Uniform](https://pbst.pinewood-builders.com/handbook/#uniform)\n" +
                    "- [Ranktag](https://pbst.pinewood-builders.com/handbook/#ranktag)\n" +
                    "- [Going off duty](https://pbst.pinewood-builders.com/handbook/#going-off-duty)\n" +
                    "- [Usage of weapons](https://pbst.pinewood-builders.com/handbook/#usage-of-weapons)\n" +
                    "- [How to deal with rulebreakers](https://pbst.pinewood-builders.com/handbook/#how-to-deal-with-rulebreakers)\n" +
                    "- [Kill on Sight (KoS)](https://pbst.pinewood-builders.com/handbook/#kill-on-sight-kos)\n" +
                    "[Important facilities to patrol](https://pbst.pinewood-builders.com/handbook/#important-facilities-to-patrol)\n" +
                    "- [Pinewood Computer Core](https://pbst.pinewood-builders.com/handbook/#pinewood-computer-core)\n" +
                    "- [PBST Activity Center (PBSTAC)](https://pbst.pinewood-builders.com/handbook/#pbst-activity-center-pbstac)\n" +
                    "[Other groups you may encounter](https://pbst.pinewood-builders.com/handbook/#other-groups-you-may-encounter)\n" +
                    "- [Pinewood Emergency Team](https://pbst.pinewood-builders.com/handbook/#pinewood-emergency-team)\n" +
                    "- [The Mayhem Syndicate](https://pbst.pinewood-builders.com/handbook/#the-mayhem-syndicate)\n" +
                    "[Trainings and ranking up](https://pbst.pinewood-builders.com/handbook/#trainings-and-ranking-up)\n" +
                    "- [Training](https://pbst.pinewood-builders.com/handbook/#training)\n" +
                    "[Ranks](https://pbst.pinewood-builders.com/handbook/#ranks)\n" +
                    "- [Tiers](https://pbst.pinewood-builders.com/handbook/#tiers)\n" +
                    "- [Tier 4 (aka, Special Defence, SD)](https://pbst.pinewood-builders.com/handbook/#tier-4-aka-special-defence-sd)\n" +
                    "- [Trainers](https://pbst.pinewood-builders.com/handbook/#trainers)\n").setFooter("This embed is owned by: " + c.getMember().getNickname()).buildEmbed()).queue();
                break;
            case "TMSHandbook":
                m.editMessage(c.makeEmbeddedMessage(Color.DARK_GRAY).setDescription("**[TMS Handbook:](https://tms.pinewood-builders.com/tms/)**\n" +
                    " - [Ranking System](https://tms.pinewood-builders.com/tms/#ranking-system)\n" +
                    " - [General Rules](https://tms.pinewood-builders.com/tms/#general-rules)\n" +
                    " - [Ranking up](https://tms.pinewood-builders.com/tms/#ranking-up)\n" +
                    "    - [Trooper](https://tms.pinewood-builders.com/tms/#trooper)\n" +
                    "    - [Operative](https://tms.pinewood-builders.com/tms/#operative)\n" +
                    "    - [Captain](https://tms.pinewood-builders.com/tms/#captain)\n" +
                    "    - [Instructor](https://tms.pinewood-builders.com/tms/#instructor)").setFooter("This embed is owned by: " + c.getMember().getNickname()).buildEmbed()).queue();
                break;
            case "PETHandbook":
                m.editMessage(c.makeEmbeddedMessage(Color.RED).setDescription("**[PET Handbook:](https://pet.pinewood-builders.com/pet/)**\n" +
                    " - [General Rules](https://pet.pinewood-builders.com/pet/#general-rules)\n" +
                    " - [Trainers and Chiefs Rules](https://pet.pinewood-builders.com/pet/#trainers-and-chiefs-rules)\n" +
                    " - [Manager Rules](https://pet.pinewood-builders.com/pet/#manager-rules)\n\n" +
                    "**Other Divisons**:\n" +
                    " - <:PETFire:690205071476129897> (**Fire Division**)\n" +
                    " - <:PETHazmat:690205016753045649> (**Hazmat Division**)\n" +
                    " - <:PETMedic:690204927385272374> (**Medic Division**)").setFooter("This embed is owned by: " + c.getMember().getNickname()).buildEmbed()).queue();
                addPETReactions(m);
                startPETWaiter(m, c);
                break;
            default:
                m.editMessage(c.makeError(":no_entry_sign: This emoji is invalid. Please execute the command again and select a correct emoji.").buildEmbed()).queue();
                break;

        }
    }


    public static boolean isMessage(String messageId, String reactMessageId) {
        return messageId.equals(reactMessageId);
    }

    private void addReactions(Message m) {
        try {
            TimeUnit.SECONDS.sleep(2);
            m.addReaction("PBSTHandbook:690133745805819917").queue();
            TimeUnit.SECONDS.sleep(1);
            m.addReaction("TMSHandbook:690134424041422859").queue();
            TimeUnit.SECONDS.sleep(1);
            m.addReaction("PETHandbook:690134297465585704").queue();
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void addPETReactions(Message m) {
        try {
            TimeUnit.SECONDS.sleep(3);
            m.addReaction("PETFire:690205071476129897").queue();
            TimeUnit.SECONDS.sleep(1);
            m.addReaction("PETHazmat:690205016753045649").queue();
            TimeUnit.SECONDS.sleep(1);
            m.addReaction("PETMedic:690204927385272374").queue();
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
