package com.avairebot.commands.pinewood;

import com.avairebot.AvaIre;
import com.avairebot.Constants;
import com.avairebot.chat.PlaceholderMessage;
import com.avairebot.commands.CommandMessage;
import com.avairebot.contracts.commands.Command;
import com.avairebot.contracts.commands.CommandGroup;
import com.avairebot.contracts.commands.CommandGroups;
import com.avairebot.database.collection.Collection;
import com.avairebot.database.collection.DataRow;
import com.jagrosh.jdautilities.menu.Paginator;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.avairebot.pinewood.utils.database.ReportDatabaseRequests.getReportsOnRobloxId;
import static com.avairebot.utils.JsonReader.readJsonFromUrl;

public class SearchReportedUserCommand extends Command {
    public SearchReportedUserCommand(AvaIre avaire) {
        super(avaire);
    }

    @Override
    public String getName() {
        return "Search Reported User Command";
    }

    @Override
    public String getDescription() {
        return "See the reports per division, or all divisions of a roblox user.";
    }

    @Override
    public List <String> getUsageInstructions() {
        return Collections.singletonList(
            "`:command` - Start the search system."
        );
    }

    @Override
    public List <String> getExampleUsage() {
        return Collections.singletonList(
            "`:command` - Start the search system."
        );
    }


    @Override
    public List <String> getTriggers() {
        return Arrays.asList("search-reported-user", "sru");
    }

    @Nonnull
    @Override
    public List <CommandGroup> getGroups() {
        return Collections.singletonList(
            CommandGroups.REPORTS
        );
    }

    @Override
    public List <String> getMiddleware() {
        return Arrays.asList("isOfficialPinewoodGuild",
            "throttle:guild,1,30",
            "isBotAdmin"
        );
    }

    @Override
    public boolean onCommand(CommandMessage context, String[] args) {
        if (args.length < 1) {
            context.makeError("What user are you looking for? (Roblox ID (<:numbers:695500330460250133>) or Username (<:custom_username:695501219275210773>))").queue(p -> {
                p.addReaction("username:695501219275210773").queue();
                // p.addReaction("numbers:695500330460250133").queue();
                avaire.getWaiter().waitForEvent(GuildMessageReactionAddEvent.class, j -> {
                    if (j.getUser().isBot()) return false;
                    if (j.getChannel().equals(context.getChannel()) && j.getMember().equals(context.member)) {
                        return j.getReactionEmote().getName().equals("custom_username") || j.getReactionEmote().getName().equals("numbers");
                    }
                    return false;
                }, j -> {
                    if ("custom_username".equals(j.getReactionEmote().getName())) {
                        p.editMessage(context.makeInfo("What is the username?").buildEmbed()).queue();
                        j.getChannel().retrieveMessageById(j.getMessageId()).complete().clearReactions().queue();
                        usernameWaiter(context);
                    }
                });
            });
        }
        if (args.length > 0) {
            if (args[0].equals("punishment")) {
                if (!args[1].matches("^[0-9]*$")) {
                    context.makeError("Please use a number for the Case ID.").queue();
                    return false;
                }
                try {
                    Collection collection = avaire.getDatabase().newQueryBuilder(Constants.REPORTS_DATABASE_TABLE_NAME).where("id", args[1]).get();
                    String message = String.join(" ", Arrays.copyOfRange(args, 2, args.length))
                        .replaceAll("\\\\n", "\n");

                    avaire.getDatabase().newQueryBuilder(Constants.REPORTS_DATABASE_TABLE_NAME).where("id", args[1]).update(statement -> statement.set("report_punishment", message, true));

                    context.makeInfo("I've added the punishment ``:punishment`` onto ``" + collection.get(0).getString("reported_roblox_name") + "'s`` report info.").set("punishment", message).queue();

                    List <Member> users = context.guild.getMembersByEffectiveName(collection.get(0).getString("reported_roblox_name"), true);

                    if (users.size() == 1) {
                        users.get(0).getUser().openPrivateChannel().queue(v -> v.sendMessage("Hello! My name is " + context.getJDA().getSelfUser().getName() + " and have to inform you about the punishment you've gotten by: ``" + context.getMember().getEffectiveName() + ("``\n" +
                            "**Your punishment**: ```:punishment```").replace(":punishment", message)).queue());
                    } else {
                        context.makeError("The amount of users with the same name is more or less then 1, so no info message has been sent!").queue();
                    }
                    context.guild.getTextChannelsByName("handbook-violator-reports", true).get(0).retrieveMessageById(collection.get(0).getString("report_message_id")).queue(p -> p.editMessage(createEmbedAndBuild(collection, context, message)).queue());
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            } else {
                context.makeError("Please use the correct arguments for this system.\n - ``c!sru punishment <report id> <what punishment has been set>``").queue();
            }
        }
        return false;
    }

    private MessageEmbed createEmbedAndBuild(Collection collection, CommandMessage context, String punish) {
        String evidence = collection.get(0).getString("report_evidence");

        PlaceholderMessage eb = context.makeEmbeddedMessage(returnColor(context.guild.getIdLong()));

        if (isEmbedFile(evidence)) {
            eb.setDescription(
                "**Report on**: [" + collection.get(0).getString("reported_roblox_name") + "](https://roblox.com/users/" + getRobloxId(collection.get(0).getString("reported_roblox_name")) + ")\n" +
                    "**Information**: \n" + collection.get(0).getString("report_reason") + "\n" +
                    "**Time and date**: " + collection.get(0).getString("report_date") + "\n" +
                    "**Punishment**: ```" + punish + "```\n" +
                    "**Evidence**: \n"

            ).setImage(evidence);
        } else {
            if (!(evidence.contains(" ") || evidence.contains("\n"))) {
                eb.setDescription(
                    "**Report on**: [" + collection.get(0).getString("reported_roblox_name") + "](https://roblox.com/users/" + getRobloxId(collection.get(0).getString("reported_roblox_name")) + ")\n" +
                        "**Information**: \n" + collection.get(0).getString("report_reason") + "\n" +
                        "**Time and date**: " + collection.get(0).getString("report_date") + "\n" +
                        "**Evidence**: [Click here to open](" + evidence + ")" + "\n\n**Punishment**: ```" + punish + "```"
                );
            } else {
                eb.setDescription(
                    "**Report on**: [" + collection.get(0).getString("reported_roblox_name") + "](https://roblox.com/users/" + getRobloxId(collection.get(0).getString("reported_roblox_name")) + ")\n" +
                        "**Information**: \n" + collection.get(0).getString("report_reason") + "\n" +
                        "**Time and date**: " + collection.get(0).getString("report_date") + "\n" +
                        "**Evidence**: \n" + evidence + "\n**Punishment**: ```" + punish + "```"
                );
            }

        }

        eb.setThumbnail("http://www.roblox.com/Thumbs/Avatar.ashx?x=256&y=256&Format=Png&username=" + collection.get(0).getString("reported_roblox_name"))
            .setTitle("Reported by: " + context.member.getEffectiveName()).buildEmbed();
        return eb.buildEmbed();
    }

    private void usernameWaiter(CommandMessage context) {
        avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, c -> {
            if (c.getChannel().equals(context.getChannel()) && c.getMember().equals(context.getMember())) {
                if (!isValidRobloxUser(c.getMessage().getContentRaw())) {
                    context.makeError("This user isn't a user I can find on roblox. Please try again (Type the name again)").queue();
                    return false;
                }
                //context.makeError("The roblox user is valid!").queue();

                return true;

            }
            //context.makeError("The channel or member isn't the same").queue();

            return false;
        }, c -> {
            int roblox_id = getRobloxId(c.getMessage().getContentRaw());
            if (getReportsOnRobloxId(roblox_id) > 0) {
                try {
                    Collection collection = avaire.getDatabase().newQueryBuilder(Constants.REPORTS_DATABASE_TABLE_NAME).where("reported_roblox_id", roblox_id).get();

                    Paginator.Builder pb = new Paginator.Builder().setItemsPerPage(1).waitOnSinglePage(true).useNumberedItems(false).setEventWaiter(avaire.getWaiter())
                        .setText(EmbedBuilder.ZERO_WIDTH_SPACE).setTimeout(1, TimeUnit.MINUTES).setUsers(context.member.getUser()).setFinalAction(message -> {
                            if (context.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_MANAGE)) {
                                message.clearReactions().queue();
                            }
                        });
                    List<String> pages = new ArrayList<>();
                    collection.getItems().stream().map(coll -> "**User report**: ``" + c.getMessage().getContentRaw() + "``\n\n" +
                        "**Reporter**: ``" + coll.getString("reporter_discord_name") + "`` (<@" + coll.getLong("reporter_discord_id") + ">)\n" +
                        "**Saved evidence**: \n" + coll.getString("report_evidence") + "\n" +
                        "**Saved information**: \n" + coll.getString("report_reason", null) + "\n" +
                        "**Saved date**: ``" + coll.getString("report_date") + "``\n" +
                        "**Punishment**: \n" + (coll.getString("report_punishment") != null ? coll.getString("report_punishment", null) : "Currently no punishment given.") + "\n\n" +
                        "**Report ID**: ``" + coll.getString("id") + "``\n" +
                        "**Reported in**: ``"+ context.getJDA().getGuildById(coll.getLong("pb_server_id")).getName() + "``")
                        .forEach(pb::addItems);


                    Paginator p = pb.setUsers(context.getAuthor()).build();

                    p.paginate(context.channel, 1);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                context.makeInfo("This user doesn't have any reports on his account.").queue();
            }
        });
    }


    private Color returnColor(DataRow coll) {
        if (coll.getLong("pb_server_id") == 438134543837560832L) { //PBST
            return new Color(33, 78, 179);
        }
        if (coll.getLong("pb_server_id") == 572104809973415943L) { //TMS
            return new Color(120, 120, 120);
        }
        if (coll.getLong("pb_server_id") == 436670173777362944L) { //PET
            return new Color(170, 0, 0);
        }
        return new Color(128, 247, 255);
    }

    private boolean isEmbedFile(String proof) {
        return proof.endsWith(".png") ||
            proof.endsWith(".jpg") ||
            proof.endsWith(".gif") ||
            proof.endsWith(".tif") ||
            proof.endsWith(".jpeg") && !proof.contains(" ");
    }



    private Color returnColor(long server_id) {
        if (server_id == 438134543837560832L) { //PBST
            return new Color(33, 78, 179);
        }
        if (server_id == 572104809973415943L) { //TMS
            return new Color(120, 120, 120);
        }
        if (server_id == 436670173777362944L) { //PET
            return new Color(170, 0, 0);
        }
        return new Color(128, 247, 255);
    }

    public boolean isValidRobloxUser(String un) {

        try {
            JSONObject json = readJsonFromUrl("http://api.roblox.com/users/get-by-username?username=" + un);
            String username = json.getString("Username");

            return username != null;
        } catch (Exception e) {
            return false;
        }
    }

    public int getRobloxId(String un) {

        try {
            JSONObject json = readJsonFromUrl("http://api.roblox.com/users/get-by-username?username=" + un);
            return json.getInt("Id");
        } catch (Exception e) {
            return 0;
        }
    }

}
